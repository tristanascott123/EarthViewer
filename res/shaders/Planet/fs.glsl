#version 410 core

in vec3 vFragPos_FS_in;
in vec3 vOrigPos_FS_in;
in vec2 vTexCoords_FS_in;

out vec4 FragColor;

uniform vec3 uSunDir = vec3(0,0,-1);

uniform samplerCube mAlbetoSampler;

vec4 GenerateTextureCoords(sampler2D samp, vec3 pos, float scale) {
    vec2 xy = pos.xy * scale;
    vec2 xz = pos.xz * scale;
    vec2 yz = pos.yz * scale;

    vec3 blending = abs(pos);
    blending = normalize(max(blending, 0.00001));
    float b = (blending.x + blending.y + blending.z);
    blending /= vec3(b,b,b);

    vec4 xaxis = texture(samp, yz);
    vec4 yaxis = texture(samp, xz);
    vec4 zaxis = texture(samp, xy);

    vec4 tex = xaxis * blending.x + yaxis * blending.y + zaxis * blending.z;

    return vec4(tex.xyz, 1);
}

float clamp(float src, float l, float u) {
    if(src < l) return l;
    if(src > u) return u;
    return src;
}

void main() {
    vec3 nNormal = normalize(vFragPos_FS_in);
    float fact = dot(nNormal, uSunDir);
    fact = clamp(fact, 0.1, 1);

    FragColor = fact * texture(mAlbetoSampler, normalize(vOrigPos_FS_in));
}