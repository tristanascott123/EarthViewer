#version 410 core

layout (location = 0) in vec3 aPosition_VS_in;
layout (location = 1) in vec2 aTexCoords_VS_in;

out vec3 vFragPos_CS_in;
out vec2 vTexCoords_CS_in;

out vec3 vSunDir_CS_in;

uniform mat4 uModelMatrix = mat4(1.0);

void main() {
    vFragPos_CS_in = (uModelMatrix * vec4(aPosition_VS_in, 1)).xyz;
    vTexCoords_CS_in = aTexCoords_VS_in;
}
