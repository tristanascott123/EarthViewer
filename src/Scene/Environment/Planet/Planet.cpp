#include "Planet.hpp"

#include "Utilities/GeneralMath.hpp"

Planet::Planet()
    : mPosition(0, 0, 0),
      mRotation(23, 0, 0)
{
}

Planet::~Planet()
{
}

auto Planet::Update(Timestep timestep) -> void
{
  float speed = 1.0f / 25.0f;
  mRotation.y += speed * timestep * 60 * 6.28f;
}

auto Planet::GenerateModelMatrix() -> glm::mat4
{
  const auto translation = glm::translate(glm::mat4(1.0f), mPosition);
  const auto rotation = gm::Rotate(mRotation);
  return translation * rotation;
}