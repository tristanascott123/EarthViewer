#include "BasicTexture.hpp"

#include <GL/glew.h>

auto BasicTexture::Bind(int type, int loc) const -> void
{
    glActiveTexture(GL_TEXTURE0 + loc);
    glBindTexture(type, mTextureHandle);
}

auto BasicTexture::Unbind() const -> void
{
    glBindTexture(GL_TEXTURE_2D, 0);
}